package com.abc.onlinejobportal.dto;

import java.util.Objects;

public class AccountResponseDTOUser {
	private String userName;
	private int PhoneNumber;
	private String Password;
	
	public AccountResponseDTOUser() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AccountResponseDTOUser(String userName, int phoneNumber, String Password) {
		super();
		userName = userName;
		PhoneNumber = phoneNumber;
	}
	
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		userName = userName;
	}
	public int getPhoneNumber() {
		return PhoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		PhoneNumber = phoneNumber;
	}
	@Override
	public int hashCode() {
		return Objects.hash( Password, PhoneNumber, userName);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountResponseDTOUser other = (AccountResponseDTOUser) obj;
		return Objects.equals(Password, other.Password)
				&& PhoneNumber == other.PhoneNumber && Objects.equals(userName, other.userName);
	}

	
	
}
