package com.abc.onlinejobportal.service;

import org.springframework.security.core.userdetails.User;

public interface UserService {

	//User loadUserByUsername(String username);
    void save(User user);

    User findByUsername(String username);

	User findByUsername1(String username);

	User loadUserByUsername(String username);

	void save1(User user);

	com.abc.onlinejobportal.entity.User searchUser(int id);

	boolean verifyforRegistration(String username, String password);

	String login(String username, String password);
}