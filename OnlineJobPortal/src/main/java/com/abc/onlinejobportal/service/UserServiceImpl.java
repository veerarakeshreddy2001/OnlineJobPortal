package com.abc.onlinejobportal.service;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.abc.onlinejobportal.repository.JobRepository;
import com.abc.onlinejobportal.repository.UserRepository;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JobRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save1(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(JpaRepository.findAll()));
        userRepository.save(user);
    }

    @Override
    public User findByUsername1(String username) {
        return userRepository.findByUsername(username);
    }

	@Override
	public void save(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User loadUserByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public com.abc.onlinejobportal.entity.User searchUser(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean verifyforRegistration(String username, String password) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String login(String username, String password) {
		// TODO Auto-generated method stub
		return null;
	}
}