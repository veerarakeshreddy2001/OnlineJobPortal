package com.abc.onlinejobportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abc.onlinejobportal.entity.Job;


public interface JobRepository extends JpaRepository<Job ,Long>
{
	 Job findByJobname(String job);
}

