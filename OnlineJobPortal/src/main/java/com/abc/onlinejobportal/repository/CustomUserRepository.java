package com.abc.onlinejobportal.repository;


public interface CustomUserRepository {
	public String verifyUser(String username,String password)/*throws InvalidUserException*/;
	public boolean verifyEntries(String username, String password) ;

}
