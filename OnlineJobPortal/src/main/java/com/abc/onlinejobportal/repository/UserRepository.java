package com.abc.onlinejobportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abc.onlinejobportal.entity.User;

public interface UserRepository extends JpaRepository<User,Long>
{

	User findByUsername(String username);
}
