package com.abc.onlinejobportal.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.onlinejobportal.dto.LoginDTO;
import com.abc.onlinejobportal.entity.User;
import com.abc.onlinejobportal.exceptions.InvalidUserException;
import com.abc.onlinejobportal.exceptions.WrongUserOrPasswordException;
import com.abc.onlinejobportal.service.UserService;
import com.abc.onlinejobportal.validate.ValidateEntry;

@RestController
@RequestMapping("/home")
public class UserRestController {
	@Autowired
	UserService userSer;
	
	@PostMapping("/user/login")
	public boolean doLogin(@RequestBody LoginDTO loginObj,HttpServletRequest req) throws InvalidUserException,WrongUserOrPasswordException{
		String username = loginObj.getUsername();
		String password = loginObj.getPassword();
		if(ValidateEntry.validateNullEntry(username)&&ValidateEntry.validateNullEntry(password)) {
			String role = userSer.login(username, password);
			if(userSer.verifyforRegistration(username, password)) {
			
				if(role.equalsIgnoreCase("User"))
				{
				
					// generate new session 
					HttpSession session = req.getSession(true);
					
					session.setAttribute("role", role);
					session.setAttribute("username",username);
					session.setAttribute("password",password);
					return true;
					
				
				
				}
				else
				{
			
				throw new InvalidUserException("","");
				}
			
			  }
			else
			{
			
			throw new WrongUserOrPasswordException(username,password);
			}
			
			
		}
		else
		{
		
			throw new InvalidUserException("","");
		}
	}
	@GetMapping("/user/logout")
	public boolean doLogout(HttpServletRequest req)
	{
		HttpSession session = req.getSession(false);
		String role = (String)session.getAttribute("role");
		if(role.equalsIgnoreCase("User"))
		{
			session.invalidate(); // to logout
			return true;
		}
		else return false;
	}
	
	@GetMapping("/{Id}")
	public User getUser(@PathVariable int Id) {
		User obj = userSer.searchUser(Id);
		return obj;
	}
	
	
	


}
